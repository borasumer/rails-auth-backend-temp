Rails.application.routes.draw do
api_guard_routes for: 'users', controller: {
  registration: 'users/registration',
  authentication: 'users/authentication',
  passwords: 'users/passwords',
  tokens: 'users/tokens',
  }

  api_guard_scope 'users' do
  get '/user' => 'users/authentication#getuser'
end

end
